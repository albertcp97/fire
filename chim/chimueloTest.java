package hola;

import static org.junit.Assert.*;

import org.junit.Test;

public class chimueloTest {

	@Test
	public void test() {
	
		assertEquals(chimuelo.calcula(30, 10, 30, 5), "RATBOY");
		assertEquals(chimuelo.calcula(30, 5, 30, 15), "GOS");
		assertEquals(chimuelo.calcula(0, 0, 0, 0), "RATBOY");
		assertEquals(chimuelo.calcula(-15, 10, -15, 5), "RATBOY");
		assertEquals(chimuelo.calcula(30, 20, 30, 20), "RATBOY");
		assertEquals(chimuelo.calcula(30, 5, 30, 2), "RATBOY");
		assertEquals(chimuelo.calcula(100, 10, 100, 20), "GOS");
	}

}
